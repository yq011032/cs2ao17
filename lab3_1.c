# include <stdio.h>
# include <unistd.h>
# include <sys/types.h>

void main()
{
	printf("Process ID is: %d\n", getpid());
	printf("Parent process ID is: %d\n", getppid());
	sleep(30); /* sleep for 2 minutes */
	printf(" I am awake.\n");
} 
