#include <pthread.h>
#include <stdio.h>

typedef struct thread_data {
int a;
int b;
}thread_data;


void *myThread(void *arg)
{
	thread_data *tdata=(thread_data *)arg;
	int result;
	result =  tdata->a + tdata->b;
	pthread_exit((void *)result);
}

int main()
{
	pthread_t tid;
	thread_data tdata;
	void * result;
	printf("Enter the two values you want to add\n");
	scanf("%d%d", &tdata.a, &tdata.b);
	pthread_create(&tid, NULL, myThread, (void *)&tdata);
	pthread_join(tid, &result);
	printf("%d + %d = %d\n", tdata.a, tdata.b, (int)result);
	return 0;
}
