#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(void) {
int fd1[2], fd2[2];

pipe(fd1); // create pipe1
pipe(fd2); // create pipe2

if (! fork()) { //cild proces for ls 
close(fd1[0]); // close the read end of pipe1
dup2(fd1[1], STDOUT_FILENO); // redirect stdout to pipe1 write end
close(fd1[1]); //close the write end of pipe1
execlp("ls", "ls", NULL); //
} else { // Parent process
if (! fork()) { //child process for cut
close (fd1[1]); // close the write end of pipe1
dup2(fd1[0], STDIN_FILENO); // redirect the Stdin to pipe1 read end
close(fd1[0]); // close the read end of pipe1
close(fd2[0]); // close the read end of pipe2
dup2(fd2[1], STDOUT_FILENO); //redirect the stdout to pipe2 write end
close(fd2[1]);//close the write end of pipe2
execlp("cut", "cut", "-b", "1-3", NULL);
} else { // parent process
close(fd1[0]); //close read end of pipe1
close(fd1[1]); // close write end of pipe1
if (!fork()) { // child process for sort
close(fd2[1]); //close write end of pipe2
dup2(fd2[0], STDIN_FILENO); // redirect stdin to pipe2 read end
close(fd2[0]); // close read end of pipe2
execlp("sort", "sort", NULL); 
} else { // parent process 
close(fd2[0]); // close read end of pipe2
close(fd2[1]); // close write end of pipe2
wait(NULL); // wait for all child processes t finish
}
}
}
return 0; 
}

