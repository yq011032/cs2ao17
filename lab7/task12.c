#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <pthread.h>
#define N 3 // defining the total number of threads we want

float total = 0;
void *compute (void *arg) { // compute function to do something 
int i;
float result = 0;
int pid = *(int *)arg;
for (i=0; i < 2000000000; i++) // for loop 
result = sqrt(1000.0) * sqrt(1000.0);
printf("Result of %d is %f\n", pid, result); // display the result
total = total + result;// to keep a running total in the global variable total
printf("Total of %d is %f\n", pid, total); // display the running total so far
return NULL;
}

int main(){
pthread_t tid[N];
int pid[N];
int i;
for(i = 0; i < N; i++) { // for loop for creating the required number of threads
pid[i] = i;
printf("Thread Id for thread %d\n", i);
pthread_create(&tid[i], NULL, compute, &pid[i]);// creating thread
}
for (i = 0; i < N; i++) { // joining all threads
pthread_join(tid[i], NULL);
}
return 0;
}


