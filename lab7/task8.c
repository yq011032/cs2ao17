#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


int counter = 0;

int main() {
	int pid = fork();
	for (int i = 0; i<20; i++) {
	if (pid == -1) {
	perror("fork() failed");
	exit(EXIT_FAILURE);
	}

	else if (pid == 0) {
	printf("The child process counter is  ", ++counter);
	}

	else {
	printf("The parent process counter is  ", ++counter);

	}
	}

	return 0;
}
