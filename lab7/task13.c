#include <stdio.h>
#include <stdlib.h>

#define MAX_PROCESSES 50

typedef struct {
int processId;
int burstTime;
int waitingTime;
int turnaroundTime;
int type; // 1 = CPU-bound, 2 = I/O-bound, 3 = real-time 
} Process;

// three separate queues with different priority levels are created 
Process queue1[MAX_PROCESSES], queue2[MAX_PROCESSES], queue3[MAX_PROCESSES];
int lead1 = -1, end1 = -1, lead2 = -1, end2 = -1, lead3 = -1, end3 = -1;

// enqueue a process into its corresponding queue
void enqueue(Process process) {
if (process.type == 1) {
if (lead1 == -1 && end1 == -1) {
lead1++;
}
end1++;
queue1[end1] = process;
} else if (process.type == 2) {
if (lead2 == -1 && end2 == -1) {
lead2++;
}
end2++;
queue2[end2] = process;
} else if (process.type == 3) {
if (lead3 == -1 && end3 == -1) {
lead3++;
}
end3++;
queue3[end3] = process;
}
}

// A process is dequeued from the highest priority queue that is not empty 
Process dequeue(){
if (lead1 != -1 && end1 != -1) {
Process process = queue1[lead1];
if (lead1 == end1) {
lead1 = -1;
end1 = -1;
} else {
lead1++;
}
return process;
} else if (lead2 != -1 && end2 != -1) {
Process process = queue2[lead2];
if (lead2 == end2) {
lead2 = -1;
end2 = -1;
} else {
lead2++;
}
return process;
} else if (lead3 != -1 && end3 != -1) {
Process process = queue3[lead3];
if (lead3 == end3) {
lead3 = -1;
end3 = -1;
} else {
lead3++;
}
return process;
} else {
Process empty_process = {-1, 0, 0, 0, -1 };
return empty_process;
}
}

// calculating  waiting time and turnaround time for each process 
void calculate_times(Process processes[], int num_processes) {
int current_time = 0;
int completed_processes = 0;

while (completed_processes < num_processes) {
//Dequeue a process from the highest priority queue
Process current_process = dequeue();

if(current_process.processId != -1) {
//calculating  waiting time and turnaround time for the current process
current_process.waitingTime = current_time;
current_process.turnaroundTime = current_time + current_process.burstTime;
processes[current_process.processId -1] = current_process;

// current_time and completed_processes are incremented
current_time += current_process.burstTime;
completed_processes++;
} else { 
//No processes in any of the queues, increment current_time
current_time++;
}
// To check if any new processes have arrived and enqueue them 
}
}
int main() {
int num_processes;
printf("Enter the number of processes: ");
scanf("%d", &num_processes);

Process processes[MAX_PROCESSES];
for (int i = 0; i < num_processes; i++) {
Process process;
process.processId = i + 1;
printf("Enter the burst time for process %d: ", process.processId);
scanf("%d", &process.burstTime);
printf("Enter the type of process %d (1 = CPU-bound, 2 = I/O-bound, 3 = real-time): ", process.processId);
scanf("%d", &process.type);
enqueue(process);
}
calculate_times(processes, num_processes);

printf("Process\tBurst Time\tType\tWaiting Time\tTurnaround Time\n");
for (int i = 0; i < num_processes; i++) {
Process process = processes[i];
printf("%d\t%d\t\t%d\t%d\t\t%d\n", process.processId, process.burstTime, process.type, process.waitingTime, process.turnaroundTime);
}
return 0;
}


