#include <stdio.h>
#include <unistd.h>

int counter  = 0;
int main()
{

pid_t pid;
pid = fork();
for ( int i = 0; i<20; i++) {
	if (pid == -1) {
	printf("failed to fork.\n");
	return 1;
} else if (pid == 0) {
	printf("The child process counter is %d\n", ++counter);
} else  {
	printf("The parent process counter is %d\n", ++counter);
}
}
return 0;
}
