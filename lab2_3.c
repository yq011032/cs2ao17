#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>

void main()
{
	int ret = fork();
	if (ret == 0)
	{ /* this is the child process */
	  printf("The child process ID is %d\n", getpid());
	  printf("The child's parent process ID is %d\n", getppid());
	} else { /*This is the parent process*/
	  printf("The parent process ID is %d\n", getpid());
	  printf("The parent's parent process ID is %d\n", getppid());
	}
	sleep(2);
}
